from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired


# New user registration form
class SignupForm(FlaskForm):
    username = StringField('Придумайте имя пользователя:', validators=[DataRequired()])
    name = StringField('Введите ваше имя:', validators=[DataRequired()])
    password = PasswordField('Введите пароль: ', validators=[DataRequired()])


# User authorisation form
class LoginForm(FlaskForm):
    username = StringField('Имя пользователя:', validators=[DataRequired()])
    password = PasswordField('Пароль: ', validators=[DataRequired()])

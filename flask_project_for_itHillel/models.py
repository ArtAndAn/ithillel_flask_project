from database import db


# User basic model
class UserModel(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_name = db.Column(db.Text)
    name = db.Column(db.Text)
    password = db.Column(db.Text)

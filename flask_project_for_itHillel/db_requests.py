from database import db
from models import UserModel


# The function checks 'username' for uniqueness and returns True/False depending on result
def unique(new_username):
    data = UserModel.query.filter_by(user_name=new_username).one_or_none()
    return False if data else True


# The function adds a new user into the 'users' table
def insert_new_user(username, name, password):
    user = UserModel(user_name=username, name=name, password=password)
    db.session.add(user)
    db.session.commit()


# The function finds user by 'id' and returns it
def get_user_by_id(user_id):
    data = UserModel.query.filter_by(id=user_id).one_or_none()
    user = data.__dict__ if data else None
    return user


# The function finds user by 'username' and returns it
def get_user_by_username(username):
    data = UserModel.query.filter_by(user_name=username).one_or_none()
    user = data.__dict__ if data else None
    return user

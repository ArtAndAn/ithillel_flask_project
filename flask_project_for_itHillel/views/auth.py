from flask import render_template, flash, redirect, url_for, Blueprint
from flask_login import current_user, login_user, logout_user
from werkzeug.security import generate_password_hash, check_password_hash

from forms import SignupForm, LoginForm
from db_requests import unique, insert_new_user, get_user_by_username
from user import UserLogin


auth_route = Blueprint('auth', __name__)


# New users registration page
@auth_route.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if form.validate_on_submit():
        user_data = form.data
        if not unique(user_data['username']):
            flash('Похоже что пользователь с таким логином уже существует')
        else:
            pass_hash = generate_password_hash(user_data['password'])
            insert_new_user(user_data['username'], user_data['name'], pass_hash)
            return redirect(url_for('auth.login'))
    return render_template('signup.html', form=form)


# Authorisation page
@auth_route.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('base.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user_data = form.data
        user_from_db = get_user_by_username(user_data['username'])
        if user_from_db and check_password_hash(user_from_db['password'], user_data['password']):
            user_login = UserLogin().create(user_from_db)
            login_user(user_login)
            return redirect(url_for('base.index'))
        flash('Неверное имя пользователя или пароль')
    return render_template('login.html', form=form)


# Logout page
@auth_route.route('/logout')
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('auth.login'))
    logout_user()
    return redirect('/')

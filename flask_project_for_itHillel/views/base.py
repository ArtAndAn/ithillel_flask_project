from flask import Blueprint, render_template
from flask_login import current_user

from db_requests import get_user_by_id


base_route = Blueprint('base', __name__)


# Main page
@base_route.route('/')
def index():
    return render_template('index.html',
                           auth=current_user.is_authenticated,
                           user=get_user_by_id(current_user.get_id()))

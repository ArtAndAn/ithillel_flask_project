from flask import Blueprint, render_template


error_route = Blueprint('error', __name__)


# 500 error page
@error_route.app_errorhandler(500)
def internal_error(error):
    return render_template('errors/500.html')

from flask import Flask
from flask_login import LoginManager

from config import FlaskConfig
from database import db
from user import UserLogin
from views import base, auth, error


# Site application initialisation, adding config
def create_app():
    app = Flask(__name__)
    app.config.from_object(FlaskConfig)

    db.init_app(app)
    db.create_all(app=app)

    return app


app = create_app()

# Application route blueprints registration
app.register_blueprint(base.base_route)
app.register_blueprint(auth.auth_route)
app.register_blueprint(error.error_route)

# Manager for users authorisation / registration
login_manager = LoginManager(app)


# User loader for login manager
@login_manager.user_loader
def load_user(user_id):
    return UserLogin().fromDB(user_id)
